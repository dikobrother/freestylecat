using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathLine : MonoBehaviour
{
    [SerializeField] private Spawner _spawner;
    [SerializeField] private int _lives;
    [SerializeField] private UIController _uIController;

    private void Start()
    {
        _uIController.UpdateLives(_lives);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Figure figure))
        {
            _spawner.DeleteSpawnedFigure(figure);
            figure.KillFigure();
            _lives--;
            _uIController.UpdateLives(_lives);
            if(_lives <= 0)
            {
                _spawner.EndSpawn();
                _uIController.ShowEndWindow();
            }
        }
    }
}
