using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureSelector : MonoBehaviour
{
    [SerializeField] private Spawner _spawner;
    [SerializeField] private int _scoreForTap;
    [SerializeField] private List<AudioSource> _meowSources;
    [SerializeField] private GameObject _openedMouth;
    [SerializeField] private UIController _uIController;
    private IEnumerator _openMouth;
    private int score = 0;

    private void Awake()
    {
        _uIController.UpdateScore(score);
    }

    public void OnFigureButtonClicked(int index)
    {
        if((int)_spawner.GetFirstElement().GetFigureType() != index)
        {
            return;
        }
        Figure figure = _spawner.GetFirstElement();
        score += _scoreForTap;
        _uIController.UpdateScore(score);
        _meowSources[index].Play();
        if(_openMouth == null)
        {
            _openMouth = OpenMouth();
            StartCoroutine(_openMouth);
        }
        _spawner.DeleteSpawnedFigure(figure);
        figure.KillFigure();

    }

    private IEnumerator OpenMouth()
    {
        _openedMouth.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        _openedMouth.SetActive(false);
        _openMouth = null;
    }
}
