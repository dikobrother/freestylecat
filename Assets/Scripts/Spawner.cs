using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] List<Figure> _figures;
    [SerializeField] private float _minSpawnTime;
    [SerializeField] private float _maxSpawnTime;
    [SerializeField] private List<Figure> _spawnedFigures;
    [SerializeField] private List<AudioSource> _musicSources;
    private IEnumerator _spawnCoroutine;

    private void Start()
    {
        StartSpawn();
        int randomMusic = Random.Range(0, _musicSources.Count);
        _musicSources[randomMusic].Play();
    }

    public void StartSpawn()
    {
        if(_spawnCoroutine == null)
        {
            _spawnCoroutine = SpawnCoroutine();
            StartCoroutine(_spawnCoroutine);
        }       
    }

    public void EndSpawn()
    {
        if(_spawnCoroutine != null)
        {
            StopCoroutine(_spawnCoroutine);
            _spawnCoroutine = null;
        }
        foreach (var item in _spawnedFigures)
        {
            item.KillFigure();
        }
        _spawnedFigures.Clear();
        foreach (var item in _musicSources)
        {
            item.Stop();
        }
    }

    private IEnumerator SpawnCoroutine()
    {
        int index = Random.Range(0, _figures.Count);
        float cooldown = Random.Range(_minSpawnTime, _maxSpawnTime); 
        Figure figure = Instantiate(_figures[index], transform);
        _spawnedFigures.Add(figure);
        yield return new WaitForSeconds(cooldown);
        _spawnCoroutine = null;
        StartSpawn();
    }

    public void DeleteSpawnedFigure(Figure figure)
    {
        _spawnedFigures.Remove(figure);
    }

    public Figure GetFirstElement()
    {
        return _spawnedFigures[0];
    }
}
