using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour
{
    [SerializeField] private FigureTypes.Types _type;
    [SerializeField] private float _speed;
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private List<Color> _colors;
    private bool _isAlive = true;
    private IEnumerator _movingCoroutine;

    public FigureTypes.Types GetFigureType()
    {
        return _type;
    }

    private void Start()
    {
        _sprite.color = _colors[Random.Range(0, _colors.Count)];
        StartMoving();
    }

    private void StartMoving()
    {
        _movingCoroutine = MovingCoroutine();
        StartCoroutine(_movingCoroutine);
    }

    public void StopMoving()
    {
        if(_movingCoroutine != null)
        {
            StopCoroutine(_movingCoroutine);
            _movingCoroutine = null;
        }
    }

    public void KillFigure()
    {
        StopMoving();
        Destroy(gameObject);
    }

    private IEnumerator MovingCoroutine()
    {
        while (_isAlive)
        {
            transform.Translate(Vector2.down * _speed * Time.deltaTime);
            yield return null;
        }
    }
}
