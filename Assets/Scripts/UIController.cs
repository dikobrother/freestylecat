using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    [SerializeField] private TMP_Text _scoreText;
    [SerializeField] private TMP_Text _livesText;
    [SerializeField] private GameObject _endCanvas;
    [SerializeField] private GameObject _meowButtons;

    public void UpdateScore(int score)
    {
        _scoreText.text = score.ToString();
    }

    public void UpdateLives(int lives)
    {
        _livesText.text = lives.ToString();
    }

    public void ShowEndWindow()
    {
        _endCanvas.SetActive(true);
        _meowButtons.SetActive(false);
    }

    public void GoHome()
    {
        SceneManager.LoadScene(0);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
    }
}
