using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private Color _offMusic;
    [SerializeField] private Color _onMusic;
    [SerializeField] private Image _soundImage;
    [SerializeField] private Button _soundButton;
    private float _minVolume = -80f;
    private float _maxVolume = 0f;

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void CloseApp()
    {
        Application.Quit();
    }

    private void Start()
    {
        if (!PlayerPrefs.HasKey("Music"))
        {
            PlayerPrefs.SetInt("Music", 1);
        }
        UpdateAudio();
    }

    public void UpdateAudio()
    {

        if (PlayerPrefs.GetInt("Music") == 1)
        {
            TurnOnMusic();
        }
        else
        {
            TurnOffMusic();
        }
    }

    public void TurnOffMusic()
    {
        _audioMixer.SetFloat("MusicVolume", _minVolume);
        PlayerPrefs.SetInt("Music", 0);
        _soundImage.color = _offMusic;
        _soundButton.onClick.RemoveAllListeners();
        _soundButton.onClick.AddListener(TurnOnMusic);
    }

    public void TurnOnMusic()
    {
        _audioMixer.SetFloat("MusicVolume", _maxVolume);
        PlayerPrefs.SetInt("Music", 1);
        _soundImage.color = _onMusic;
        _soundButton.onClick.RemoveAllListeners();
        _soundButton.onClick.AddListener(TurnOffMusic);
    }

}
